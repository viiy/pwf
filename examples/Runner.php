<?php

namespace Examples;

use VIT\PWF\Interfaces\RunnerInterface;

class Runner implements RunnerInterface
{
    use \VIT\PWF\Traits\RunnerTrait;
}
