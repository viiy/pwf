<?php

require_once __DIR__.'/../vendor/autoload.php';

$wf = new \Examples\MyFirstWorkflow\MyFirstWorkflow;

$firstStep = new \Examples\MyFirstWorkflow\Steps\FirstStep('firstStepId');
$secondStep = new \Examples\MyFirstWorkflow\Steps\SecondStep('secondStepId');
$finalStep = new \Examples\MyFirstWorkflow\Steps\FinalStep('finalStepId');

$fromFirstToSecondTransition = new \Examples\MyFirstWorkflow\Transitions\FromFirstToSecondTransition('fromFirstToSecondTransitionId');
$fromFirstToSecondTransitionHandler = new \Examples\MyFirstWorkflow\TransitionHandlers\TransitionHandler;
$fromFirstToSecondTransition
    ->addTransitionFromSteps($firstStep)
    ->setTransitionToStep($secondStep)
    ->setCondition(function () {
        return true;
    })
    ->setTransitionHandler($fromFirstToSecondTransitionHandler);

$fromSecondToFinalTransition = new \Examples\MyFirstWorkflow\Transitions\FromSecondToFinalTransition('fromSecondToFinalTransition');
$fromSecondToFinalTransitionHandler = new \Examples\MyFirstWorkflow\TransitionHandlers\TransitionHandler;
$fromSecondToFinalTransition
    ->addTransitionFromSteps($secondStep)
    ->setTransitionToStep($finalStep)
    ->setCondition(function () {
        return true;
    })
    ->setTransitionHandler($fromSecondToFinalTransitionHandler);

$fromAnyToFinalTransition = new \Examples\MyFirstWorkflow\Transitions\FromAnyToFinalTransition('fromAnyToFinalTransition');
$fromAnyToFinalTransitionHandler = new \Examples\MyFirstWorkflow\TransitionHandlers\TransitionHandler;
$fromAnyToFinalTransition
    ->setTransitionToStep($finalStep)
    ->setCondition(function () {
        return true;
    })
    ->setTransitionHandler($fromAnyToFinalTransitionHandler);

$wf->setFirstStep($firstStep)->addTransition($fromFirstToSecondTransition)->addTransition($fromSecondToFinalTransition)->addTransition($fromAnyToFinalTransition);

//var_dump($wf->getTransitions());exit();
$runner = new \Examples\Runner($wf);
$runner->currentStep = $firstStep;

//var_dump($runner->getAvailableTransitions());
try {
    $runner->transitToStep($finalStep);
} catch (\VIT\PWF\Exceptions\UnavailableTransitionException $exception) {
    echo $exception->getMessage();
}
