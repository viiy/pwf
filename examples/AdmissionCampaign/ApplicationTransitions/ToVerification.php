<?php

namespace Examples\AdmissionCampaign\ApplicationTransitions;

use Examples\AdmissionCampaign\ApplicationState;
use VIT\PWF\Collections\StepsCollection;
use VIT\PWF\Interfaces\RunnerInterface;
use VIT\PWF\Interfaces\StepInterface;
use VIT\PWF\Interfaces\TransitionInterface;

class ToVerification implements TransitionInterface
{
    public function getId(): string|int
    {
        return 'from-draft-to-verification';
    }

    public function getFromSteps(): ?StepsCollection
    {
        return StepsCollection::make([ApplicationState::Draft, ApplicationState::Revision]);
    }

    public function getToStep(): StepInterface
    {
        return ApplicationState::Verification;
    }

    public function allowed(RunnerInterface $runner): bool
    {
        // TODO: Implement allowed() method.
    }

    public function handle(RunnerInterface $runner): RunnerInterface
    {
        // TODO: Implement handle() method.
    }
}
