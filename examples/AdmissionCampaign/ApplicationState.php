<?php

namespace Examples\AdmissionCampaign;

use VIT\PWF\Interfaces\StepInterface;

enum ApplicationState: string implements StepInterface
{
    case Draft = 'draft';
    case Verification = 'verification';
    case Revision = 'revision';
    case Verified = 'verified';
    case Diagnostic = 'diagnostic';
    case DiagnosticCompleted = 'diagnostic_completed';

    public function getStepId(): string|int
    {
        return $this->value;
    }
}
