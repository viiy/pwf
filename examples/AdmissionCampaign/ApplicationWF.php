<?php

namespace Examples\AdmissionCampaign;

use VIT\PWF\Interfaces\WorkflowInterface;
use VIT\PWF\Traits\WorkflowTrait;

class ApplicationWF implements WorkflowInterface
{
    use WorkflowTrait;

    public function __construct() {}
}
