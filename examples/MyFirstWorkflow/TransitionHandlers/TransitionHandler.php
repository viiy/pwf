<?php

namespace Examples\MyFirstWorkflow\TransitionHandlers;

use VIT\PWF\Interfaces\RunnerInterface;
use VIT\PWF\Interfaces\TransitionHandlerInterface;
use VIT\PWF\Interfaces\TransitionInterface;

class TransitionHandler implements TransitionHandlerInterface
{
    public function execute(RunnerInterface $runner, TransitionInterface $transition, ...$params): RunnerInterface
    {
        $runner->setCurrentStep($transition->getToStep());

        return $runner;
    }
}
