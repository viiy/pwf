<?php

namespace Examples\MyFirstWorkflow\Transitions;

use VIT\PWF\Interfaces\RunnerInterface;
use VIT\PWF\Interfaces\TransitionInterface;
use VIT\PWF\Traits\TransitionTrait;

class FromAnyToFinalTransition implements TransitionInterface
{
    use TransitionTrait;

    public function __construct(protected readonly string $id) {}

    public function getId(): string|int
    {
        return $this->id;
    }

    public function handle(RunnerInterface $runner, ...$params): RunnerInterface {}
}
