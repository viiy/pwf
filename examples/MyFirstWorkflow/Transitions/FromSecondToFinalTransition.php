<?php

namespace Examples\MyFirstWorkflow\Transitions;

use VIT\PWF\Interfaces\TransitionInterface;
use VIT\PWF\Traits\TransitionTrait;

class FromSecondToFinalTransition implements TransitionInterface
{
    use TransitionTrait;

    public function __construct(protected readonly string $id) {}

    public function getId(): string|int
    {
        return $this->id;
    }
}
