<?php

namespace Examples\MyFirstWorkflow;

use VIT\PWF\Interfaces\WorkflowInterface;
use VIT\PWF\Traits\WorkflowTrait;

class MyFirstWorkflow implements WorkflowInterface
{
    use WorkflowTrait;
}
