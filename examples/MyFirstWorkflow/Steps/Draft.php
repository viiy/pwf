<?php

namespace Examples\MyFirstWorkflow\Steps;

use VIT\PWF\Interfaces\StepInterface;

enum ApplicationSteps: string implements StepInterface
{
    case DRAFT = 'draft';

    public function getStepId(): string|int
    {
        return $this->value;
    }
}
