<?php

namespace Examples\MyFirstWorkflow\Steps;

use VIT\PWF\Interfaces\StepInterface;

class SecondStep implements StepInterface
{
    public function __construct(public readonly string $id) {}

    #[\Override]
    public function getStepId(): string|int
    {
        return $this->id;
    }
}
