<?php

namespace VIT\PWF\Exceptions;

use Exception;
use VIT\PWF\Interfaces\StepInterface;
use VIT\PWF\Interfaces\WorkflowInterface;

class UnavailableTransitionException extends Exception
{
    public readonly WorkflowInterface $workflow;

    public readonly StepInterface $fromStep;

    public readonly StepInterface $toStep;

    /**
     * @throws UnavailableTransitionException
     */
    public static function throw(WorkflowInterface $workflow, StepInterface $fromStep, StepInterface $toStep): void
    {
        $self = new static('Unavailable transition');
        $self->workflow = $workflow;
        $self->fromStep = $fromStep;
        $self->toStep = $toStep;
        throw $self;
    }
}
