<?php

namespace VIT\PWF\Collections;

use VIT\PWF\Interfaces\StepInterface;

class StepsCollection
{
    /**
     * @var StepInterface[]
     */
    private array $stepIndex = [];

    /**
     * @param  StepInterface|StepInterface[]  $step
     * @return $this
     */
    public function push(StepInterface|array $step): static
    {
        if (is_array($step)) {
            foreach ($step as $step_item) {
                $this->push($step_item);
            }
        } else {
            $this->stepIndex[$step->getStepId()] = $step;
        }

        return $this;
    }

    public function find(string|int $step_id): ?StepInterface
    {
        return $this->stepIndex[$step_id] ?? null;
    }

    public function get(): array
    {
        return array_values($this->stepIndex);
    }

    /**
     * @param  StepInterface|StepInterface[]|null  $steps
     */
    public static function make(StepInterface|array|null $steps = null): StepsCollection
    {
        $instance = new self;
        if (! is_null($steps)) {
            $instance->push($steps);
        }

        return $instance;
    }
}
