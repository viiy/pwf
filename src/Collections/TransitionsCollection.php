<?php

namespace VIT\PWF\Collections;

use VIT\PWF\Interfaces\StepInterface;
use VIT\PWF\Interfaces\TransitionInterface;

class TransitionsCollection
{
    private array $transitionIndex = [];

    private array $fromStepIndex = [];

    private array $toStepIndex = [];

    public function whereFromToStep(StepInterface $fromStep, ?StepInterface $toStep = null): static
    {
        $newCollection = new static;
        /** @var TransitionInterface $transition */
        foreach (array_merge(
            $this->fromStepIndex[$fromStep->getStepId()] ?? [],
            $this->fromStepIndex[null] ?? []
        ) as $transition) {
            if (is_null($toStep) || $transition->getToStep()->getStepId() === $toStep->getStepId()) {
                $newCollection->push($transition);
            }
        }

        return $newCollection;
    }

    public function push(TransitionInterface|array $transition): static
    {
        if (is_array($transition)) {
            foreach ($transition as $transition_item) {
                $this->push($transition_item);
            }
        } else {
            if (! isset($this->transitionIndex[$transition->getId()])) {
                $this->transitionIndex[$transition->getId()] = $transition;
                $fromSteps = $transition->getFromSteps();
                if (is_null($fromSteps)) {
                    $this->fromStepIndex[null][] = $transition;
                } else {
                    /** @var StepInterface $step */
                    foreach ($fromSteps as $step) {
                        $this->fromStepIndex[$step->getStepId()][] = $transition;
                    }
                }
                $this->toStepIndex[$transition->getToStep()->getStepId()][] = $transition;
            }
        }

        return $this;
    }

    /**
     * @return TransitionInterface []
     */
    public function get(): array
    {
        return array_values($this->transitionIndex);
    }

    public function empty(): bool
    {
        return empty($this->transitionIndex);
    }

    /**
     * @param  TransitionInterface|TransitionInterface[]|null  $transition
     */
    public static function make(TransitionInterface|array|null $transition = null): TransitionsCollection
    {
        $instance = new self;
        if (! is_null($transition)) {
            $instance->push($transition);
        }

        return $instance;
    }
}
