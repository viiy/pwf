<?php

namespace VIT\PWF\Traits;

use VIT\PWF\Collections\TransitionsCollection;
use VIT\PWF\Exceptions\UnavailableTransitionException;
use VIT\PWF\Interfaces\RunnerInterface;
use VIT\PWF\Interfaces\StepInterface;
use VIT\PWF\Interfaces\WorkflowInterface;

/**
 * @implements RunnerInterface
 */
trait RunnerTrait
{
    private WorkflowInterface $workflow;

    public StepInterface $currentStep;

    final public function setWorkflow(WorkflowInterface $workflow): static
    {
        $this->workflow = $workflow;

        return $this;
    }

    public function getWorkflow(): WorkflowInterface
    {
        return $this->workflow;
    }

    final public function startWorkflow(): static
    {
        $this->setCurrentStep($this->getWorkflow()->getFirstStep());

        return $this;
    }

    public function setCurrentStep(StepInterface $step): static
    {
        $this->currentStep = $step;

        return $this;
    }

    public function getCurrentStep(): StepInterface
    {
        return $this->currentStep;
    }

    final public function getAllowedTransitions(?StepInterface $toStep = null): TransitionsCollection
    {
        $allowedTransitionsCollection = new TransitionsCollection;
        foreach ($this->getWorkflow()->getTransitions()->whereFromToStep($this->getCurrentStep())->get() as $transition) {
            if ($transition->allowed($this)) {
                $allowedTransitionsCollection->push($transition);
            }
        }

        return $allowedTransitionsCollection;
    }

    /**
     * @throws UnavailableTransitionException
     */
    final public function transitToStep(StepInterface $toStep, ...$params): static
    {
        $allowedTransitionsCollection = $this->getAllowedTransitions($this->getCurrentStep(), $toStep);
        if ($allowedTransitionsCollection->empty() || ! $allowedTransitionsCollection->get()[0]->allowed($this)) {
            UnavailableTransitionException::throw($this->getWorkflow(), $this->getCurrentStep(), $toStep);
        }
        $allowedTransitionsCollection->get()[0]->handle($this, ...$params);

        return $this;
    }
}
