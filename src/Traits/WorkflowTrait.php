<?php

namespace VIT\PWF\Traits;

use VIT\PWF\Collections\StepsCollection;
use VIT\PWF\Collections\TransitionsCollection;
use VIT\PWF\Interfaces\StepInterface;
use VIT\PWF\Interfaces\TransitionInterface;

trait WorkflowTrait
{
    private TransitionsCollection $transitionsCollection;

    private StepsCollection $stepsCollection;

    private StepInterface $firstStep;

    public function setFirstStep(StepInterface $step): static
    {
        $this->firstStep = $step;

        return $this;
    }

    public function getFirstStep(): StepInterface
    {
        return $this->firstStep;
    }

    public function addTransition(TransitionInterface $transition): static
    {
        $this->transitionsCollection = $this->transitionsCollection ?? new TransitionsCollection;
        $this->stepsCollection = $this->stepsCollection ?? new StepsCollection;
        $this->transitionsCollection->push($transition);
        $this->stepsCollection->push($transition->getFromSteps()->get());
        $this->stepsCollection->push($transition->getToStep());

        return $this;
    }

    public function getTransitions(): TransitionsCollection
    {
        return $this->transitionsCollection;
    }

    public function getSteps(): StepsCollection
    {
        return $this->stepsCollection;
    }

    public function checkWorkflow()
    {
        //Check that all transition fromSteps can be used
        //All transition handlers is set
    }
}
