<?php

namespace VIT\PWF\Traits;

use VIT\PWF\Collections\StepsCollection;
use VIT\PWF\Interfaces\RunnerInterface;
use VIT\PWF\Interfaces\StepInterface;
use VIT\PWF\Interfaces\TransitionInterface;

/**
 * @implements TransitionInterface
 */
trait TransitionTrait
{
    protected ?StepsCollection $transitionFromSteps = null;

    protected StepInterface $transitionToStep;

    /**
     * @var callable
     */
    protected $transitionHandler;

    /**
     * @var callable
     */
    protected $transitionConditionCallback;

    public function addTransitionFromSteps(StepInterface ...$steps): static
    {
        $this->transitionFromSteps = $this->transitionFromSteps ?? new StepsCollection;
        $this->transitionFromSteps->push($steps);

        return $this;
    }

    public function getFromSteps(): ?StepsCollection
    {
        return $this->transitionFromSteps;
    }

    public function setTransitionToStep(StepInterface $step): static
    {
        $this->transitionToStep = $step;

        return $this;
    }

    public function getToStep(): StepInterface
    {
        return $this->transitionToStep;
    }

    public function setCondition(callable $conditionCallback): static
    {
        $this->transitionConditionCallback = $conditionCallback;

        return $this;
    }

    public function allowed(RunnerInterface $runner): bool
    {
        if (is_callable($this->transitionConditionCallback)) {
            return call_user_func($this->transitionConditionCallback, $runner);
        } else {
            return true;
        }
    }
}
