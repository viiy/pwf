<?php

namespace VIT\PWF\Interfaces;

interface StepInterface
{
    public function getStepId(): string|int;
}
