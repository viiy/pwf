<?php

namespace VIT\PWF\Interfaces;

use VIT\PWF\Collections\StepsCollection;
use VIT\PWF\Collections\TransitionsCollection;

interface WorkflowInterface
{
    public function getFirstStep(): StepInterface;

    public function getTransitions(): TransitionsCollection;

    public function getSteps(): StepsCollection;
}
