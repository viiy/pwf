<?php

namespace VIT\PWF\Interfaces;

use VIT\PWF\Collections\StepsCollection;

interface TransitionInterface
{
    public function getId(): string|int;

    public function getFromSteps(): ?StepsCollection;

    public function getToStep(): StepInterface;

    public function allowed(RunnerInterface $runner): bool;

    public function handle(RunnerInterface $runner): RunnerInterface;
}
