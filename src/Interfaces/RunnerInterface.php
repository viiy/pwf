<?php

namespace VIT\PWF\Interfaces;

use VIT\PWF\Collections\TransitionsCollection;

interface RunnerInterface
{
    public function getWorkflow(): WorkflowInterface;

    public function getCurrentStep(): StepInterface;

    public function getAllowedTransitions(): TransitionsCollection;

    public function transitToStep(StepInterface $step): static;
}
